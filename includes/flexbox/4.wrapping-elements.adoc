=== 4.wrapping-elements
==== 4.1. flex-wrap: nowrap
- the default is flex-wrap: nowrap;

.css
[source, css]
----
.flex-container {
    display: flex;
}

.flex-item {
    width: 300px; /*keeps the width if it is enough space*/
}
----

image::../../assets/flexbox/4.1.png[]

==== 4.2. flex-wrap: wrap
- wraps the flex items and tries to split their heights evenly

.css
[source, css]
----
.flex-container {
    display: flex;
    flex-wrap: wrap;
}
----

image::../../assets/flexbox/4.2.png[]

==== 4.3. flex-wrap: wrap-reverse

image::../../assets/flexbox/4.3.png[]

<<<